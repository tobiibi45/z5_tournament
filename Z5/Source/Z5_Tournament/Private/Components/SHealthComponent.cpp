// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/SHealthComponent.h"
#include "Z5_Tournament.h"
#include "Net/UnrealNetwork.h"
#include "Engine/World.h"
#include "TimerManager.h"


// Sets default values for this component's properties
USHealthComponent::USHealthComponent()
{
	MaxHealth = DEFAULT_MAX_HEALTH;

	bIsDead = false;

	MyOwner = GetOwner();

	SetIsReplicated(true);

}


// Called when the game starts
void USHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwnerRole() == ROLE_Authority)
	{
		if (MyOwner)
		{
			MyOwner->OnTakeAnyDamage.AddDynamic(this, &USHealthComponent::UpdateHealth);
		}
	}

	bIsDead = false;
	Health = MaxHealth;

}

void USHealthComponent::DamageTaken(USHealthComponent* MyOwningHealthComp, float MyHealth, float MyHealthDelta, const class UDamageType* MyDamageType, class AController* MyInstigatedBy, AActor* MyDamageCauser, bool PawnIsDead)
{
	OnHealthChanged.Broadcast(MyOwningHealthComp, MyHealth, MyHealthDelta, MyDamageType, MyInstigatedBy, MyDamageCauser, PawnIsDead);
}

void USHealthComponent::UpdateHealth(AActor*DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{

	if (Damage <= 0.0f)
	{
		return;
	}

	if (GetOwnerRole() < ROLE_Authority)
	{
		ServerHandleTakeDamage(DamagedActor, Damage, DamageType, InstigatedBy, DamageCauser);
	}

	Health = FMath::Clamp(Health - Damage, 0.0f, MaxHealth);
	bIsDead = Health <= 0.0f;

	if (bIsDead)
	{
		Health = 0.0f;
	}

	OnHealthChanged.Broadcast(this, Health, Damage, DamageType, InstigatedBy, DamageCauser, bIsDead);

	if (GetOwnerRole() == ROLE_Authority)
	{
		PawnDamgeInfo.Health = Health;
		PawnDamgeInfo.HealthDelta = Damage;
		PawnDamgeInfo.bPawnIsDead = bIsDead;
	}

	OnRep_PlayerDamageInfo();

	UE_LOG(LogTemp, Log, TEXT("Health Changed: %s"), *FString::SanitizeFloat(Health));
}

void USHealthComponent::OnRep_PlayerDamageInfo()
{
	DamageTaken(this, PawnDamgeInfo.Health, PawnDamgeInfo.HealthDelta, nullptr, nullptr, nullptr, PawnDamgeInfo.bPawnIsDead);
}

void USHealthComponent::ServerHandleTakeDamage_Implementation(AActor*DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	UpdateHealth(DamagedActor, Damage, DamageType, InstigatedBy, DamageCauser);
}

bool USHealthComponent::ServerHandleTakeDamage_Validate(AActor*DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	return true;
}

void USHealthComponent::Heal(float HealAmmount)
{
	if (HealAmmount <= 0 || Health >= MaxHealth || bIsDead)
	{
		return;
	}

	if (GetOwnerRole() < ROLE_Authority)
	{
		ServerHandleHeal(HealAmmount);
	}

	Health = FMath::Clamp(Health + HealAmmount, 0.0f, MaxHealth);

	bIsDead = Health <= 0.0f;

	if (bIsDead)
	{
		Health = 0.0f;
	}

	OnHealthChanged.Broadcast(this, Health, -HealAmmount, nullptr, nullptr, nullptr, bIsDead);

	if (GetOwnerRole() == ROLE_Authority)
	{
		PawnDamgeInfo.Health = Health;
		PawnDamgeInfo.HealthDelta = HealAmmount;
		PawnDamgeInfo.bPawnIsDead = bIsDead;
	}

	OnRep_PlayerDamageInfo();

	UE_LOG(LogTemp, Log, TEXT("Health Changed: %s (+%s)"), *FString::SanitizeFloat(Health), *FString::SanitizeFloat(HealAmmount));
}

void USHealthComponent::ServerHandleHeal_Implementation(float HealAmmount)
{
	Heal(HealAmmount);
}

bool USHealthComponent::ServerHandleHeal_Validate(float HealAmmount)
{
	return true;
}

void  USHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(USHealthComponent, PawnDamgeInfo, COND_OwnerOnly);
}
