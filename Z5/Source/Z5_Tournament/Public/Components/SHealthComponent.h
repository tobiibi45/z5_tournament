// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SHealthComponent.generated.h"


//Custom On Health Change Event
DECLARE_DYNAMIC_MULTICAST_DELEGATE_SevenParams(FOnHealthChangeSignature, USHealthComponent*, HealthComp, float, Health, float, HealthDelta,
const class UDamageType*, DamageType, class AController*, InstigatedBy, AActor*, DamageCauser, bool, PawnIsDead);


//Contains PLayer Health, Damage and Death State Information
USTRUCT()
struct FTakeAnyDamage
{
	//Always add generated_body when making new struct or classes
	GENERATED_BODY()

public:
	UPROPERTY()
		float Health;

	UPROPERTY()
		float HealthDelta;

	UPROPERTY()
		bool bPawnIsDead;
};


UCLASS( ClassGroup=(Z5), meta=(BlueprintSpawnableComponent) )
class Z5_TOURNAMENT_API USHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	USHealthComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(ReplicatedUsing = OnRep_PlayerDamageInfo)
	FTakeAnyDamage PawnDamgeInfo;

	AActor* MyOwner;

	UPROPERTY(BlueprintReadOnly, Category = "HealthComponent")
	bool bIsDead;

	UPROPERTY(BlueprintReadOnly, Category = "HealthComponent")
	float Health;

	UPROPERTY(BlueprintReadOnly, Category = "HealthComponent")
	float MaxHealth;

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerHandleTakeDamage(AActor*DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION()
		void UpdateHealth(AActor*DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION()
	void OnRep_PlayerDamageInfo();

	void DamageTaken(USHealthComponent* OwningHealthComp, float Health, float HealthDelta,
	const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser, bool PawnIsDead);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerHandleHeal(float HealAmmount);

public:

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FOnHealthChangeSignature OnHealthChanged;

	UFUNCTION(BlueprintCallable, Category = "HealthComponent")
	void Heal(float HealAmmount);

};
