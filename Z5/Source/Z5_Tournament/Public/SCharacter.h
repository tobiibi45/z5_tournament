// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SCharacter.generated.h"

class UCameraComponent;
class USpringArmComponent;
class USHealthComponent;

UCLASS()
class Z5_TOURNAMENT_API ASCharacter : public ACharacter
{
	GENERATED_BODY()
protected:

	//Attaches a camera to the player for the third person controller mode
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UCameraComponent* CameraComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USpringArmComponent* SpringArmComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USHealthComponent* HealthComp;

	UPROPERTY(EditDefaultsOnly, Category = "Player")
	float ZoomedFOV;

	UPROPERTY(EditDefaultsOnly, Category = "Player", meta = (ClampMin = 0.0, ClampMax = 100))
	float  ZoomInterpSpeed;

	/*Default FOV during get play*/
	float DefaultFOV;

	bool bWantsToZoom;

	UPROPERTY(BlueprintReadOnly, Category = "Player")
	bool bCanSprint;

	UPROPERTY(BlueprintReadOnly, Category = "Player")
	bool bPlayerCanJump;

	UPROPERTY(BlueprintReadOnly, Category = "Player")
	bool bChangeStance;

	/*Pawn Died Previously*/
	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Player")
	bool bDied;

	FTimerHandle TimerHandle_ChangeStance;

public:
	// Sets default values for this character's properties
	ASCharacter();

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Moves character forward
	void MoveForward(float Value);

	//Moves character Right
	void MoveRight(float Value);

	//Allows the character to crouch
	void BeginCrouch();

	//Stops the character from crouching
	void EndCrouch();

	//Allows the character to crouch
	void BeginSprint();

	//Stops the character from crouching
	void EndSprint();

	/*Allows the player to zoom in with weapon*/
	void BeginZoom();

	/*Allows the player to stop zooming in with weapon*/
	void EndZoom();

	/*Allows the player to zoom in with weapon*/
	void StartJump();

	/*Allows the player to zoom in with weapon*/
	void EndJump();

	UFUNCTION()
	void OnHealthChanged(USHealthComponent* OwningHealthComp, float Health, float HealthDelta,
	const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser, bool PawnIsDead);

public:

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Called to get the eye location for the character
	virtual FVector GetPawnViewLocation() const override;

	// Called to get the eye location for the character
	void UpdateStance();

};
