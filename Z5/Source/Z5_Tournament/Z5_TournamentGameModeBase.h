// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Z5_TournamentGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class Z5_TOURNAMENT_API AZ5_TournamentGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
